from django.db import models

class Cat(models.Model):
    name = models.CharField(max_length=50)
    photo = models.FileField(upload_to="cats/") # así lo que haría sería guardar en una carpeta