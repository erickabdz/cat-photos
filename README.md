# Cat Picture Project
Simple project to upload cat pictures to S3 bucket using Django.
## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install the following requirements:

```bash
pip install django-storages
pip install boto3
pip install python-decouple
```

Add your own .env file with the following variables: 
```
AWS_S3_ACCESS_KEY_ID = ""
AWS_S3_SECRET_ACCESS_KEY = ""
```
You can see the app working in the capturas/ folder :) 
